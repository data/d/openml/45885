# OpenML dataset: padding-attack-dataset-2023-04-28-OpenSSL097b-2023-04-28

https://www.openml.org/d/45885

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Bleichenbacher Padding Attack: Dataset created on 2023-04-28 with server 2023-04-28-OpenSSL097b

Attribute Names:

TLS0:tcp.srcport
TLS0:tcp.dstport
TLS0:tcp.port
TLS0:tcp.stream
TLS0:tcp.len
TLS0:tcp.seq
TLS0:tcp.nxtseq
TLS0:tcp.ack
TLS0:tcp.hdr_len
TLS0:tcp.flags.res
TLS0:tcp.flags.ns
TLS0:tcp.flags.cwr
TLS0:tcp.flags.ecn
TLS0:tcp.flags.urg
TLS0:tcp.flags.ack
TLS0:tcp.flags.push
TLS0:tcp.flags.reset
TLS0:tcp.flags.syn
TLS0:tcp.flags.fin
TLS0:tcp.window_size_value
TLS0:tcp.window_size
TLS0:tcp.window_size_scalefactor
TLS0:tcp.checksum.status
TLS0:tcp.urgent_pointer
TLS0:tcp.options.nop
TLS0:tcp.option_kind
TLS0:tcp.option_len
TLS0:tcp.options.timestamp.tsval
TLS0:tcp.time_delta
TLS0:tls.record.content_type
TLS0:tls.record.length
TLS0:tls.alert_message.level
TLS0:tls.alert_message.desc
TLS0:order
DISC0:tcp.srcport
DISC0:tcp.dstport
DISC0:tcp.port
DISC0:tcp.stream
DISC0:tcp.len
DISC0:tcp.seq
DISC0:tcp.nxtseq
DISC0:tcp.ack
DISC0:tcp.hdr_len
DISC0:tcp.flags.res
DISC0:tcp.flags.ns
DISC0:tcp.flags.cwr
DISC0:tcp.flags.ecn
DISC0:tcp.flags.urg
DISC0:tcp.flags.ack
DISC0:tcp.flags.push
DISC0:tcp.flags.reset
DISC0:tcp.flags.syn
DISC0:tcp.flags.fin
DISC0:tcp.window_size_value
DISC0:tcp.window_size
DISC0:tcp.window_size_scalefactor
DISC0:tcp.checksum.status
DISC0:tcp.urgent_pointer
DISC0:tcp.options.nop
DISC0:tcp.option_kind
DISC0:tcp.option_len
DISC0:tcp.options.timestamp.tsval
DISC0:tcp.time_delta
DISC0:order
DISC1:tcp.srcport
DISC1:tcp.dstport
DISC1:tcp.port
DISC1:tcp.stream
DISC1:tcp.len
DISC1:tcp.seq
DISC1:tcp.nxtseq
DISC1:tcp.ack
DISC1:tcp.hdr_len
DISC1:tcp.flags.res
DISC1:tcp.flags.ns
DISC1:tcp.flags.cwr
DISC1:tcp.flags.ecn
DISC1:tcp.flags.urg
DISC1:tcp.flags.ack
DISC1:tcp.flags.push
DISC1:tcp.flags.reset
DISC1:tcp.flags.syn
DISC1:tcp.flags.fin
DISC1:tcp.window_size_value
DISC1:tcp.window_size
DISC1:tcp.window_size_scalefactor
DISC1:tcp.checksum.status
DISC1:tcp.urgent_pointer
DISC1:tcp.options.nop
DISC1:tcp.option_kind
DISC1:tcp.option_len
DISC1:tcp.options.timestamp.tsval
DISC1:tcp.time_delta
DISC1:order
CCS0:tcp.srcport
CCS0:tcp.dstport
CCS0:tcp.port
CCS0:tcp.stream
CCS0:tcp.len
CCS0:tcp.seq
CCS0:tcp.nxtseq
CCS0:tcp.ack
CCS0:tcp.hdr_len
CCS0:tcp.flags.res
CCS0:tcp.flags.ns
CCS0:tcp.flags.cwr
CCS0:tcp.flags.ecn
CCS0:tcp.flags.urg
CCS0:tcp.flags.ack
CCS0:tcp.flags.push
CCS0:tcp.flags.reset
CCS0:tcp.flags.syn
CCS0:tcp.flags.fin
CCS0:tcp.window_size_value
CCS0:tcp.window_size
CCS0:tcp.window_size_scalefactor
CCS0:tcp.checksum.status
CCS0:tcp.urgent_pointer
CCS0:tcp.options.nop
CCS0:tcp.option_kind
CCS0:tcp.option_len
CCS0:tcp.options.timestamp.tsval
CCS0:tcp.time_delta
CCS0:order
vulnerable_classes []

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45885) of an [OpenML dataset](https://www.openml.org/d/45885). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45885/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45885/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45885/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

